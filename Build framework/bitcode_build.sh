# Clone the depot tools
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
# Add the tools to the path
export PATH=$PATH:"`pwd`/depot_tools"
# Download the WebRTC source code
mkdir webrtc_ios
cd webrtc_ios
# This will take some time
fetch --nohooks webrtc_ios
gclient sync
# Let's start building
cd src
# Build the framework, remove --arch "arm64 x64" to build ALL architectures, including 32 bit
tools_webrtc/ios/build_ios_libs.py --bitcode
# tools_webrtc/ios/build_ios_libs.py --bitcode --arch x64 arm arm64
# The framework is at out_ios_libs/WebRTC.framework
