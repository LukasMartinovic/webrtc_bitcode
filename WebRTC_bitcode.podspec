#
# Be sure to run `pod lib lint WebRTC_bitcode.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'WebRTC_bitcode'
  s.version          = '63.11.20455'
  s.summary          = 'WebRTC framework. Build settings: --bitcode.'
  s.homepage         = 'https://github.com/lukasMH/WebRTC_bitcode'
  s.license          = { :type => 'BSD', :file => 'LICENSE' }
  s.author           = { 'lukasMH' => 'lukas.martinovic.husar@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/LukasMartinovic/webrtc_bitcode.git', :tag => s.version.to_s }
  s.ios.deployment_target = '10.0'
  s.ios.vendored_frameworks = 'WebRTC_bitcode/Frameworks/WebRTC.framework'
  s.public_header_files = 'WebRTC_bitcode/Frameworks/WebRTC.framework/Headers/*.h'
  s.requires_arc = true
end
