# WebRTC_bitcode

[![CI Status](https://img.shields.io/travis/lukasMH/WebRTC_bitcode.svg?style=flat)](https://travis-ci.org/lukasMH/WebRTC_bitcode)
[![Version](https://img.shields.io/cocoapods/v/WebRTC_bitcode.svg?style=flat)](https://cocoapods.org/pods/WebRTC_bitcode)
[![License](https://img.shields.io/cocoapods/l/WebRTC_bitcode.svg?style=flat)](https://cocoapods.org/pods/WebRTC_bitcode)
[![Platform](https://img.shields.io/cocoapods/p/WebRTC_bitcode.svg?style=flat)](https://cocoapods.org/pods/WebRTC_bitcode)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WebRTC_bitcode is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WebRTC_bitcode'
```

## Author

lukasMH, lukas.martinovic.husar@gmail.com

## License

WebRTC_bitcode is available under the MIT license. See the LICENSE file for more info.
